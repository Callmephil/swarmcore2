In this file, there'll be a To-Do list with stuff that we (the developers) need to work on, it will also be sorted out in priorities, such as MAJOR needed or minor needed etc.

#- CRASHES: (Crashes is a MAJOR priority on the server - no one like crashes and we need to fix them.
- Currently we need guild finder rewritted so that we can stop the crash from running. A temp commit has been added, that will break the handler but fix the crash until it's ready again.
- Fix other possible crashes coming up (Msoul's task to send new crashlogs when he feels like it's necessary with fresh ones that rest of the developers haven't seen)

#- Balancing of classes, (Low Priority)
This is already in progess by Jason and most of them are DB handled, (pretty LOW priority right now because it's already been processed in DB)

#- Fixing spells, (High priority)
Self-explained, if you don't know which spells/talents etc that are bugged you can see the reported ones here and get inspiration:
http://forums.wowmortal.com/bug-reports/ & http://forums.wowmortal.com/progress/ 

#- DB Errors (High priority)
We also need most of the DB errors fixed for now, so that it has less issues and faster start-up etc. 

#- Crossfaction BG (Medium - High Priority)
We need Crossfaction BG working, currently it has some issues which we need fixed as the server need Crossfaction BG badly)
One bug found:
If players relog their team mates become hostile or friendly, depending on their faction.
<<< "I relogged and this happened
- can't attack anyone
- but I can get healed
- can't talk in game either."

                                                                            ##If anything is missing feel free to update this!

                                                                                          ####www.wowmortal.com