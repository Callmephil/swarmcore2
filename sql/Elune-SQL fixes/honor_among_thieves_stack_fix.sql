REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (51698, -51698, 2, 'Honor Among Thieves (rank 1) immune to Honor Among Thieves (rank 1)');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (51700, -51700, 2, 'Honor Among Thieves (rank 2) immune to Honor Among Thieves (rank 2)');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (51701, -51701, 2, 'Honor Among Thieves (rank 3) immune to Honor Among Thieves (rank 3)');
