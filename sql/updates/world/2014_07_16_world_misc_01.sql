-- normal loot of http://www.wowhead.com/npc=39625#drops:mode=normal
DELETE FROM creature_loot_template WHERE entry=39625;
INSERT INTO `creature_loot_template` VALUES ('39625', '56116', '27', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('39625', '56115', '24', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('39625', '56112', '18', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('39625', '56113', '12', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('39625', '56114', '12', '1', '0', '1', '1');
-- heroic entry loot of  http://www.wowhead.com/npc=39625#drops:mode=heroic
DELETE FROM creature_loot_template WHERE entry=48337;
INSERT INTO `creature_loot_template` VALUES ('48337', '56443', '20', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48337', '56442', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48337', '56441', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48337', '56440', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48337', '56444', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48337', '71715', '0.5', '1', '0', '1', '1');

-- normal loot of http://www.wowhead.com/npc=40319#drops:mode=normal
DELETE FROM creature_loot_template WHERE entry=40319;
INSERT INTO `creature_loot_template` VALUES ('40319', '66927', '47', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '56126', '17', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '56123', '17', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '56127', '17', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '21525', '13', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '56125', '12', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '56124', '12', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40319', '22206', '3', '1', '0', '1', '1');

-- heroic entry loot of  http://www.wowhead.com/npc=40319#drops:mode=heroic
DELETE FROM creature_loot_template WHERE entry=48784;
INSERT INTO `creature_loot_template` VALUES ('48784', '56452', '18', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '56451', '18', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '56450', '17', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '56453', '17', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '56454', '17', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '21525', '16', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '66927', '14', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48784', '22206', '5', '1', '0', '1', '1');

-- normal loot of http://www.wowhead.com/npc=40177#drops:mode=normal
DELETE FROM creature_loot_template WHERE entry=40177;
INSERT INTO `creature_loot_template` VALUES ('40177', '56120', '25', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40177', '56118', '20', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40177', '56121', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40177', '56122', '18', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40177', '56119', '12', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40177', '71638', '1.9', '1', '0', '1', '1');

-- heroic entry loot of  http://www.wowhead.com/npc=40177#drops:mode=heroic
DELETE FROM creature_loot_template WHERE entry=48702;
INSERT INTO `creature_loot_template` VALUES ('48702', '56447', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48702', '56448', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48702', '56445', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48702', '56449', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('48702', '56446', '19', '1', '0', '1', '1');

--  stats  fixed for http://www.wowhead.com/npc=39392
DELETE FROM `creature_template` WHERE `entry`=39392 LIMIT 1;
INSERT INTO `creature_template` ( `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES ( 39392, 0, 0, 0, 0, 0, 31903, 0, 0, 0, "Faceless Corruptor", "", "", 0, 85, 85, 3, 16, 16, 0, 1.0, 0.912699, 1.0, 1, 892, 1071, 0, 924, 3.2, 2000, 2000, 1, 64, 0, 0, 0, 0, 0, 0, 892, 1071, 924, 10, 72, 39392, 0, 0, 0, 0, 0, 0, 0, 0, 75755, 75569, 79467, 0, 0, 0, 0, 0, 0, 0, 12136, 12136, "SmartAI", 0, 3, 4.54208, 1.0, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "mob_corrupteur_sans_visage", "13623");

-- normal loot of http://www.wowhead.com/npc=40586#drops:mode=normal
DELETE FROM creature_loot_template WHERE entry=40586;
INSERT INTO `creature_loot_template` VALUES ('40586', '55202', '25', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40586', '55203', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40586', '55195', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40586', '55198', '12', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40586', '55201', '12', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40586', '71636', '4', '1', '0', '1', '1');

-- heroic entry loot of  http://www.wowhead.com/npc=40586#drops:mode=heroic
DELETE FROM creature_loot_template WHERE entry=49079;
INSERT INTO `creature_loot_template` VALUES ('49079', '56269', '20', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49079', '56270', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49079', '56266', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49079', '56267', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49079', '56268', '19', '1', '0', '1', '1');

-- normal loot of http://www.wowhead.com/npc=40765#drops:mode=normal
DELETE FROM creature_loot_template WHERE entry=40765;
INSERT INTO `creature_loot_template` VALUES ('40765', '55228', '29', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40765', '55204', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40765', '55206', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40765', '55205', '13', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('40765', '55207', '12', '1', '0', '1', '1');

-- heroic entry loot of  http://www.wowhead.com/npc=40765#drops:mode=heroic
DELETE FROM creature_loot_template WHERE entry=49064;
INSERT INTO `creature_loot_template` VALUES ('49064', '56273', '20', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49064', '56271', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49064', '56274', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49064', '56275', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49064', '56272', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49064', '71715', '0.4', '1', '0', '1', '1');

-- wrong stats  fixed for http://www.wowhead.com/npc=40936
DELETE FROM `creature_template` WHERE `entry`=40936 LIMIT 1;
INSERT INTO `creature_template` ( `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES ( 40936, 49074, 0, 0, 0, 0, 31674, 0, 0, 0, "Faceless Watcher", "", "", 0, 85, 85, 3, 14, 14, 0, 1.0, 0.912699, 1.0, 1, 729, 881, 0, 805, 7.9, 2000, 2000, 1, 64, 0, 0, 0, 0, 0, 0, 729, 881, 805, 10, 8, 40936, 0, 0, 0, 0, 0, 0, 0, 0, 76590, 76604, 0, 0, 0, 0, 0, 0, 0, 0, 12163, 12163, "SmartAI", 0, 3, 18.6283, 0.0, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "", "13623");

-- wrong stats  fixed for http://www.wowhead.com/npc=44658
DELETE FROM `creature_template` WHERE `entry`=44658 LIMIT 1;
INSERT INTO `creature_template` ( `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES ( 44658, 49068, 0, 0, 0, 0, 34352, 0, 0, 0, "Deep Murloc Invader", "", "", 0, 85, 85, 3, 26, 26, 0, 1.0, 1.14286, 1.0, 0, 545, 723, 0, 839, 3.0, 2000, 2000, 1, 0, 0, 0, 0, 0, 0, 0, 545, 723, 839, 10, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 3, 0.2581, 0.0, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "", "13623");

-- wrong stats  fixed for http://www.wowhead.com/npc=44648
DELETE FROM `creature_template` WHERE `entry`=44648 LIMIT 1;
INSERT INTO `creature_template` ( `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES ( 44648, 49104, 0, 0, 0, 0, 32018, 0, 0, 0, "Unyielding Behemoth", "", "", 0, 85, 85, 3, 26, 26, 0, 1.0, 1.14286, 1.0, 1, 509, 683, 0, 805, 8.2, 2000, 2000, 1, 0, 0, 0, 0, 0, 0, 0, 509, 683, 805, 10, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83985, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 3, 8.2793, 0.0, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "npc_behemot", "13623");

-- wrong stats  fixed for http://www.wowhead.com/npc=44715
DELETE FROM `creature_template` WHERE `entry`=44715 LIMIT 1;
INSERT INTO `creature_template` ( `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES ( 44715, 49107, 0, 0, 0, 0, 32708, 0, 0, 0, "Vicious Mindlasher", "", "", 0, 85, 85, 3, 26, 26, 0, 1.0, 1.14286, 1.0, 1, 509, 683, 0, 805, 7.5, 2000, 2000, 2, 0, 0, 0, 0, 0, 0, 0, 509, 683, 805, 10, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83915, 83914, 83926, 0, 0, 0, 0, 0, 0, 0, 0, 0, "SmartAI", 0, 3, 4.1396, 2.0612, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "", "13623");

-- wrong stats  fixed for http://www.wowhead.com/npc=
DELETE FROM `creature_template` WHERE `entry`=44752 LIMIT 1;
INSERT INTO `creature_template` ( `entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES ( 44752, 49073, 0, 0, 0, 0, 32708, 0, 0, 0, "Faceless Sapper", "", "", 0, 85, 85, 3, 26, 26, 0, 1.0, 0.912699, 1.0, 1, 509, 683, 0, 805, 5.9, 2000, 2000, 1, 4, 0, 0, 0, 0, 0, 0, 509, 683, 805, 10, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83463, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 3, 8.2793, 0.0, 1.0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "", "13623");

-- normal loot of http://www.wowhead.com/npc=43214#drops:mode=normal
DELETE FROM creature_loot_template WHERE entry=43214;
INSERT INTO `creature_loot_template` VALUES ('43214', '55799', '25', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('43214', '55797', '20', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('43214', '55801', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('43214', '55798', '13', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('43214', '63043', '0.7', '1', '0', '1', '1');

-- heroic entry loot of  http://www.wowhead.com/npc=43214#drops:mode=heroic
DELETE FROM creature_loot_template WHERE entry=49538;
INSERT INTO `creature_loot_template` VALUES ('49538', '56334', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49538', '56336', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49538', '56335', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49538', '56333', '19', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49538', '63043', '0.9', '1', '0', '1', '1');
INSERT INTO `creature_loot_template` VALUES ('49538', '71715', '0.6', '1', '0', '1', '1');

