#include "SpellsExtension.h"
#include "SpellAuras.h"
#include "Player.h"
#include "Spell.h"
#include "SpellAuras.h"
#include "SpellAuraDefines.h"
#include "SpellAuraEffects.h"

Spells_UnitExtension::Spells_UnitExtension(Unit* unit) :_unit(unit) { }
Spells_UnitExtension::~Spells_UnitExtension() { }
// Disable Spell Effect DBC
/*void SpellMgr::SpellsExtension_SpellMgr(SpellInfo* spellInfo)
{
	if (!spellInfo)
		return;

	switch (spellInfo->Id)
	{
	case 12289: // Improved Hamstring rank 1
	case 12668: // Improved Hamstring rank 2
		spellInfo->Effects[EFFECT_0].TriggerSpell = 0;
		break;
	case 105788: // T13 2P - Mage
	case 105765: // T13 2P - Paladin
		spellInfo->Effects[EFFECT_0].TriggerSpell = 0;
		break;

	case 100403: // Moonwell Chalice
		spellInfo->RangeEntry = sSpellRangeStore.LookupEntry(13);  // 50000yd
		break;
	default:
		break;
	}
}
*/
/*
void SpellMgr::SpellsExtension_ModAura(SpellInfo* spellInfo)
{
	if (!spellInfo)
		return;
}
*/
bool AuraEffect::SpellsExtension_ApplicationStackAura(AuraApplication const* aurApp, uint8 mode, bool apply) const
{
	Unit* m_caster = GetCaster();
	Unit* target = aurApp->GetTarget();

	if (!target || !m_caster)
		return true;
	return true;
}

void AuraEffect::SpellsExtension_TriggerAuraTick(Unit* target, Unit* caster) const
{
	if (!caster || !target)
		return;
}

bool Spell::SpellsExtension_TriggerSpell_BEGIN(Unit* unit)
{
	if (!m_caster || !unit)
		return true;

	/* FOR TESTING ONLY 
	if (Player* playercast = m_caster->ToPlayer()) {
		sLog->outError(LOG_FILTER_NETWORKIO, "Auras Applied %u by the player %s", m_spellInfo->Id, playercast->GetName().c_str());
		sLog->outError(LOG_FILTER_WARDEN, "Auras Applied %u by the player %s", m_spellInfo->Id, playercast->GetName().c_str());
		return true;
	}
	*/

	switch (m_spellInfo->SpellFamilyName)
	{
	case SPELLFAMILY_PRIEST:
	{
		switch (m_spellInfo->Id)
		{
		case 2050:  // Heal
		case 2060:  // Greater heal
		case 2061:  // Flash Heal
		case 47750: // Penance Heal
			if (m_caster->HasAura(47516)) // Grace Rank 1
				m_caster->AddAura(47930, unit); // Grace Aura Rank 1
			if (m_caster->HasAura(47517)) // Grace Rank 2
				m_caster->AddAura(77613, unit); // Grace Aura Rank 2
			break;
		}
		break;
	}
	break;
	}

	return true;
}

bool Spell::SpellsExtension_TriggerSpell_END(Unit* victim)
{
	if (!m_caster || !victim)
		return true;

	switch (m_spellInfo->SpellFamilyName)
	{
	case SPELLFAMILY_WARLOCK:
		switch (m_spellInfo->Id)
		{
		case 6353: // Warlock T13
			int32 amount = 1;
			if (m_casttime < 1 && m_caster->HasAura(105787)) // hacky but it works after few hours
				m_caster->ModifyPower(POWER_SOUL_SHARDS, amount);
			break;
		}
		break;
	case SPELLFAMILY_MAGE:
	{
		switch (m_spellInfo->Id)
		{
		case 30451: // Arcane Blast has 100 % chance to grant stolen time
			if (m_caster->HasAura(105788)) // T13 - Mage 2P
				m_caster->CastSpell(m_caster, 105785, true); // Stolen Time (Rank 1)
			if (m_caster->HasAura(105790)) // T13 - 4P Bonus
				m_caster->AddAura(105791, m_caster); // Stolen Time (Rank 2)
			break;
			// Others 50 % 
		case 44614: // FrostFire Bolt
		case 11366: // Pyroblast
		case 133:	// Fireball
		case 116:	// Frostbolt
			if (m_caster->HasAura(105788)) // T13 - Mage 2P
			{
				if (roll_chance_i(50)) // 50 % random between 0 AND 50
				{
					m_caster->CastSpell(m_caster, 105785, true); // Stolen Time (Rank 1)
					if (m_caster->HasAura(105790)) // T13 - 4P Bonus
						m_caster->AddAura(105791, m_caster); // Stolen Time (Rank 2)
				}
			}
			break;
		case 11129: // Combustion needed here
			if (m_caster->HasAura(105791))  // While casted
				m_caster->RemoveAura(105791); // Mage T13 - 4P Bonus Remove
			break;
		}
		break;
	}
	case SPELLFAMILY_SHAMAN:
	{
		switch (m_spellInfo->Id)
		{
		case 45284: // Trigger hiden Effect Lightning bolt
		case 45297: // Trigger hiden Effect Lightning Chain
		case 77451: // Trigger Hiden Effect Lava burst
			if (m_caster->HasAura(105816)) // T13 Elemental - Shaman 4P
				m_caster->AddAura(105821, m_caster); // Time Rupture
			break;
		}
		break;
	}
	}
	return true;
}

void Spells_UnitExtension::Spells_TriggerDamage(Unit* victim, SpellInfo const* spellProto, uint32 &damage)
{
	if (!victim || !spellProto)
		return;
}