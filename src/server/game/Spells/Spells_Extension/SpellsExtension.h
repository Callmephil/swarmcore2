#ifndef _SPELLSEXTENSION_H
#define _SPELLSEXTENSION_H

#include "Player.h"
#include "Unit.h"
#include "ScriptMgr.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GameEventMgr.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "GameObject.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "InstanceScript.h"
#include "CombatAI.h"
#include "PassiveAI.h"
#include "Chat.h"
#include "DBCStructure.h"
#include "DBCStores.h"
#include "ObjectMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

class Spells_UnitExtension {
	public:
		Spells_UnitExtension(Unit* unit);
		~Spells_UnitExtension();
		/* Auras */
			uint8 Spells_GetStackAmount(uint32 spellid);
			uint8 Spells_GetCharges(uint32 spellid);
			void Spells_AddCharges(int8 incr,uint32 spellid);
			void Spells_AddStack(int8 incr,uint32 spellid);
		/* Status */
			Unit* Spells_GetMaster();
		/* SpellExtension */
			void Spells_TriggerAbsorb(Unit* victim, SpellInfo const* spellProto, uint32 &absorb);
			void Spells_TriggerHeal(Unit* victim, SpellInfo const* spellProto, uint32 &heal);
			void Spells_TriggerDamage(Unit* victim, SpellInfo const* spellProto, uint32 &damage);
			void Spells_TriggerDispellAura(Aura* aura,uint32 spellId, uint64 casterGUID, Unit* dispeller, uint8 chargesRemoved);
			void Spells_Interruption(Unit* unitTarget);
			float Spells_NegativeAuraModifier(AuraType aura) const;
			void Spells_EventMeleeAttack(Unit* victim,uint32 dmg);
			void Spells_GestionKill(Unit* victim,SpellInfo const* spellProto);
			void Spells_DeclenchementDegatsSpell(SpellNonMeleeDamage* damageInfo,SpellInfo const* spellInfo);
			void Spells_CriticSpell(Unit* victim,SpellInfo const* spellInfo);
			uint8 Spells_StackedAuraCure(DispelType type);
	private:
		Unit* _unit;
};

#endif