#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"

#define Horde		"|TInterface\\ICONS\\PVPCurrency-Conquest-Horde:24:24:0:0|t  [Horde]"
#define Alliance	"|TInterface\\ICONS\\PVPCurrency-Conquest-Alliance:24:24:0:0|t  [Alliance]"
// Male
#define Class_Dwarf		"|TInterface\\ICONS\\Achievement_Character_Dwarf_Male:24:24:0:0|t  [Dwarf]"
#define Class_Human		"|TInterface\\ICONS\\Achievement_Character_Human_Male:24:24:0:0|t  [Human]"
#define Class_Gnome		"|TInterface\\ICONS\\Achievement_Character_Gnome_Male:24:24:0:0|t  [Gnome]"
#define Class_Worgen	"|TInterface\\ICONS\\inv_mask_08:24:24:0:0|t  [Worgen]"
#define Class_Draenei	"|TInterface\\ICONS\\Achievement_Character_Draenei_Male:24:24:0:0|t  [Draenei]"
#define Class_Night_Elf "|TInterface\\ICONS\\Achievement_Character_Nightelf_Male:24:24:0:0|t  [Night Elf]"
// Male
#define Class_Orc	 "|TInterface\\ICONS\\Achievement_Character_Orc_Male:24:24:0:0|t  [Orc]"
#define Class_Troll  "|TInterface\\ICONS\\Achievement_Character_Troll_Male:24:24:0:0|t  [Troll]"
#define Class_Undead "|TInterface\\ICONS\\Achievement_Character_Undead_Male:24:24:0:0|t  [Undead]"
#define Class_Tauren "|TInterface\\ICONS\\Achievement_Character_Tauren_Male:24:24:0:0|t  [Tauren]"
#define Class_Goblin	"|TInterface\\ICONS\\inv_mask_10:24:24:0:0|t  [Goblin]"
#define Class_Blood_elf "|TInterface\\ICONS\\Achievement_Character_Bloodelf_Male:24:24:0:0|t  [Blood Elf]"
// Females
#define Class_Dwarf_f		"|TInterface\\ICONS\\Achievement_Character_Dwarf_Female:24:24:0:0|t  [Dwarf]"
#define Class_Human_f		"|TInterface\\ICONS\\Achievement_Character_Human_Female:24:24:0:0|t  [Human]"
#define Class_Gnome_f		"|TInterface\\ICONS\\Achievement_Character_Gnome_Female:24:24:0:0|t  [Gnome]"
#define Class_Worgen_f	"|TInterface\\ICONS\\inv_mask_09:24:24:0:0|t  [Worgen]"
#define Class_Draenei_f	"|TInterface\\ICONS\\Achievement_Character_Draenei_Female:24:24:0:0|t  [Draenei]"
#define Class_Night_Elf_f "|TInterface\\ICONS\\Achievement_Character_Nightelf_Female:24:24:0:0|t  [Night Elf]"
// Females
#define Class_Orc_f	 "|TInterface\\ICONS\\Achievement_Character_Orc_Female:24:24:0:0|t  [Orc]"
#define Class_Troll_f  "|TInterface\\ICONS\\Achievement_Character_Troll_Female:24:24:0:0|t  [Troll]"
#define Class_Undead_f "|TInterface\\ICONS\\Achievement_Character_Undead_Female:24:24:0:0|t  [Undead]"
#define Class_Tauren_f "|TInterface\\ICONS\\Achievement_Character_Tauren_Female:24:24:0:0|t  [Tauren]"
#define Class_Goblin_f	"|TInterface\\ICONS\\inv_mask_07:24:24:0:0|t  [Goblin]"
#define Class_Blood_elf_f "|TInterface\\ICONS\\Achievement_Character_Bloodelf_Female:24:24:0:0|t  [Blood Elf]"


#define Remove_Morph "|TInterface\\ICONS\\SPELL_HOLY_DISPELMAGIC:24:24:0:0|t  [Remove Morph]"

#define Males " - [Males]"
#define FeMales " - [Females]"	

#define BACK "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:24:24:0:0|t  |cff6E353A Return|r"
#define MAIN_MENU "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:24:24:0:0|t  |cff6E353A Main Menu|r"

enum eIcons
{
	NO_DOTS_ICON = -1
};

class Morph_npc : public CreatureScript
{
public:
	Morph_npc() : CreatureScript("Morph_npc") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->isInCombat() || player->isInFlight())
		{
			player->GetSession()->SendNotification("You are in Combat");
			return true;
		}

		uint32 unallowedspells[7] =
		{
			16595, 40120, 33943, 783, 5487, 24858, 33891
		};

		for (uint32 i = 0; i < sizeof(unallowedspells) / sizeof(uint32); i++)
		{ 
			if (player->HasAura(unallowedspells[i]))
				player->RemoveAura(unallowedspells[i]);
		}

		player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Horde, GOSSIP_SENDER_MAIN, 38);
		player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Alliance, GOSSIP_SENDER_MAIN, 39);

		player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Remove_Morph, GOSSIP_SENDER_MAIN, 37);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}
	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32  actions)
	{
		player->PlayerTalkClass->ClearMenus();

		if (sender == GOSSIP_SENDER_MAIN)
		{
			switch (actions)
			{
			case 0:
				OnGossipHello(player, creature);
				break;

			case 39: // Alliance
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Human, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Gnome, GOSSIP_SENDER_MAIN, 2);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Dwarf, GOSSIP_SENDER_MAIN, 3);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Night_Elf, GOSSIP_SENDER_MAIN, 4);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Draenei, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Worgen, GOSSIP_SENDER_MAIN, 6);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

				// Alliances
			case 1:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 39);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON,Class_Human Males, GOSSIP_SENDER_MAIN, 35);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON,Class_Human_f FeMales, GOSSIP_SENDER_MAIN, 36);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 2:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 39);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Gnome Males, GOSSIP_SENDER_MAIN, 13);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Gnome_f FeMales, GOSSIP_SENDER_MAIN, 14);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 3:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 39);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Dwarf Males, GOSSIP_SENDER_MAIN, 15);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Dwarf_f FeMales, GOSSIP_SENDER_MAIN, 16);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 4:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 39);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Night_Elf Males, GOSSIP_SENDER_MAIN, 17);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Night_Elf_f FeMales, GOSSIP_SENDER_MAIN, 18);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 5:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 39);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Draenei Males, GOSSIP_SENDER_MAIN, 19);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Draenei_f FeMales, GOSSIP_SENDER_MAIN, 20);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;


			case 6:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 39);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Worgen Males, GOSSIP_SENDER_MAIN, 21);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Worgen_f FeMales, GOSSIP_SENDER_MAIN, 22);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 38: // Horde
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Orc, GOSSIP_SENDER_MAIN, 7);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Undead, GOSSIP_SENDER_MAIN, 8);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Tauren, GOSSIP_SENDER_MAIN, 9);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Troll, GOSSIP_SENDER_MAIN, 10);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Blood_elf, GOSSIP_SENDER_MAIN, 11);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Goblin, GOSSIP_SENDER_MAIN, 12);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

				// Horde
			case 7:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 38);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Orc Males, GOSSIP_SENDER_MAIN, 23);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Orc_f FeMales, GOSSIP_SENDER_MAIN, 24);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 8:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 38);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Undead Males, GOSSIP_SENDER_MAIN, 25);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Undead_f FeMales, GOSSIP_SENDER_MAIN, 26);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 9:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 38);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Tauren Males, GOSSIP_SENDER_MAIN, 27);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Tauren_f FeMales, GOSSIP_SENDER_MAIN, 28);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 10:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 38);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Troll Males, GOSSIP_SENDER_MAIN, 29);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Troll_f FeMales, GOSSIP_SENDER_MAIN, 30);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 11:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 38);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Blood_elf Males, GOSSIP_SENDER_MAIN, 31);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Blood_elf_f FeMales, GOSSIP_SENDER_MAIN, 32);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 12:
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, MAIN_MENU, GOSSIP_SENDER_MAIN, 0);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, BACK, GOSSIP_SENDER_MAIN, 38);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Goblin Males, GOSSIP_SENDER_MAIN, 33);
				player->ADD_GOSSIP_ITEM(NO_DOTS_ICON, Class_Goblin_f FeMales, GOSSIP_SENDER_MAIN, 34);
				player->SEND_GOSSIP_MENU(1, creature->GetGUID());
				break;

			case 37: // Demorph
				player->CLOSE_GOSSIP_MENU();
				player->DeMorph();
				break;

				// Alliances 
			case 35://Human Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(19723);
				break;

			case 36://Human Females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(19724);
				break;

			case 13://Gnome Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20580);
				break;

			case 14://Gnome Females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20581);
				break;

			case 15://dwarf Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20317);
				break;

			case 16://dwarf females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37918);
				break;

			case 17:// Night Elfs Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20318);
				break;

			case 18:// Night Elfs Females
				player->SetDisplayId(37919);
				break;

			case 19:// Draenei Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37916);
				break;

			case 20:// Draenei Females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20323);
				break;

			case 21://Worgen males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37915);
				break;

			case 22://Worgen females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37914);
				break;
				///////////////////////////////////////////////// Horde
			case 23: // Orc Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37920);
				break;

			case 24://Orc females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20316);
				break;

			case 25://Undead Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37923);
				break;

			case 26:// Undead Females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37924);
				break;

			case 27://Tauren Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20585);
				break;

			case 28://Tauren Females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20584);
				break;

			case 29://Troll Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20321);
				break;

			case 30://Troll feMales
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37922);
				break;

			case 31://blood elf Males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20578);
				break;

			case 32://blood elf feMales
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(20579);
				break;

			case 33:// Goblins males
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37927);
				break;

			case 34:// Goblins females
				player->CLOSE_GOSSIP_MENU();
				player->SetDisplayId(37928);
				break;

			}
		}

		return true;
	}
};

void AddSC_Morph_npc()
{
	new Morph_npc;
}